# Userscripts

This repository contains a collection of userscripts with tweaks for various websites, designed to be used with Tampermonkey or a similar web browser plugin.