// ==UserScript==
// @name         Wanikani local time
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Script that lets you view review times in a format you prefer on wanikani.com.
// @author       db314
// @match        https://www.wanikani.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=wanikani.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.addEventListener("turbo:frame-render", async (event) => {
        document.getElementsByClassName('review-forecast__hour-title').forEach(forecast_hour => {
            let [hours, period] = forecast_hour.textContent.split(' ');
            hours = parseInt(hours)
            if (period.toLowerCase() === 'pm') {
                hours += 12;
            } else if (period.toLowerCase() === 'am' && hours === 12) {
                hours = 0;
            }
            const date = new Date()
            date.setUTCHours(hours, 0, 0, 0)
            forecast_hour.textContent = date.toLocaleTimeString("pl-PL", {timeZone: "Europe/Warsaw"}).slice(0,5)
        })
    }, { once: true });
})();
