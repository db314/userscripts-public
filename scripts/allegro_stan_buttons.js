// ==UserScript==
// @name         Allegro "Stan" filter bulk select buttons
// @namespace    http://tampermonkey.net/
// @version      2024-07-11
// @description  Add 3 buttons that allows you to select all checkboxes filters that match używane or uszkodzone "Stan" or checks in one click.
// @author       db314
// @match        https://allegro.pl/listing?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=allegro.pl
// @grant        none
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';

    let buttonUzywane, buttonUszkodzone, buttonClear;

    function clickExpandValuesButton() {
        const expandButton = document.querySelector('button[data-analytics-interaction-label="expandValues"]');
        if (expandButton) {
            expandButton.click();
        }
    }

    function createButton(text, color, onClick) {
        const button = document.createElement('button');
        button.textContent = text;
        button.style.backgroundColor = color;
        button.style.color = 'white';
        button.style.border = 'none';
        button.style.padding = '5px 10px';
        button.style.marginLeft = '10px';
        button.style.cursor = 'pointer';
        button.addEventListener('click', onClick);
        return button;
    }

    function toggleCheckboxes(fieldset, includeLabels, excludeLabels) {
        const checkboxes = fieldset.querySelectorAll('input[type="checkbox"]');
        let allSelected = true;
        checkboxes.forEach(checkbox => {
            const label = checkbox.nextElementSibling;
            if (label && ((includeLabels && includeLabels.includes(label.textContent.trim().toLowerCase())) || (!includeLabels && !excludeLabels.includes(label.textContent.trim().toLowerCase())))) {
                if (!checkbox.checked) {
                    allSelected = false;
                }
            }
        });
        checkboxes.forEach(checkbox => {
            const label = checkbox.nextElementSibling;
            if (label && ((includeLabels && includeLabels.includes(label.textContent.trim().toLowerCase())) || (!includeLabels && !excludeLabels.includes(label.textContent.trim().toLowerCase())))) {
                if (allSelected || !checkbox.checked) {
                    checkbox.click();
                }
            }
        });
    }

    function checkAndDisableButton(fieldset, labels, button) {
        const checkboxes = fieldset.querySelectorAll('input[type="checkbox"]');
        let hasMatchingCheckbox = false;
        checkboxes.forEach(checkbox => {
            const label = checkbox.nextElementSibling;
            if (label && labels.includes(label.textContent.trim().toLowerCase())) {
                hasMatchingCheckbox = true;
            }
        });
        if (!hasMatchingCheckbox) {
            button.style.backgroundColor = 'gray';
            button.disabled = true;
        }
    }

    function addButton() {
        const fieldset = document.querySelector('fieldset[data-analytics-view-value="11323"]');
        if (!fieldset) {
            return;
        }

        if (!buttonUzywane) {
            buttonUzywane = createButton('używane', '#3366ff', () => {
                clickExpandValuesButton();
                toggleCheckboxes(fieldset, null, ['nowe', 'uszkodzone', 'do renowacji', 'na części']);
            });
            checkAndDisableButton(fieldset, ['używane'], buttonUzywane);
        }

        if (!buttonUszkodzone) {
            buttonUszkodzone = createButton('uszkodzone', '#cc6600', () => {
                clickExpandValuesButton();
                toggleCheckboxes(fieldset, ['uszkodzone', 'do renowacji', 'na części'], []);
            });
            checkAndDisableButton(fieldset, ['uszkodzone', 'do renowacji', 'na części'], buttonUszkodzone);
        }

        if (!buttonClear) {
            buttonClear = createButton('clear', '#990000', () => {
                clickExpandValuesButton();
                const checkboxes = fieldset.querySelectorAll('input[type="checkbox"]');
                checkboxes.forEach(checkbox => {
                    if (checkbox.checked) {
                        checkbox.click();
                    }
                });
            });
        }

        const h3Element = fieldset.querySelector('h3');
        const divElement = fieldset.querySelector('div');

        if (h3Element && divElement) {
            if (!fieldset.contains(buttonUzywane)) {
                fieldset.insertBefore(buttonUzywane, divElement);
            }
            if (!fieldset.contains(buttonUszkodzone)) {
                fieldset.insertBefore(buttonUszkodzone, divElement);
            }
            if (!fieldset.contains(buttonClear)) {
                fieldset.insertBefore(buttonClear, divElement);
            }
        }
    }

    const observer = new MutationObserver(() => {
        addButton();
    });

    observer.observe(document, { childList: true, subtree: true });
})();
