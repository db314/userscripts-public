// ==UserScript==
// @name         OPNsense always show full list
// @namespace    http://tampermonkey.net/
// @version      2024-07-08
// @description  Automatically clicks the "All" button to always show full list of items. Remember to customize "@match" to fit your needs.
// @author       db314
// @match        https://10.0.0.1/ui/*
// @match        https://192.168.0.1/ui/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=opnsense.org
// @grant        none
// @run-at document-start
// ==/UserScript==

(function() {
    'use strict';
    function clickAllButtons() {
        const buttons = document.querySelectorAll('a[data-action="-1"].dropdown-item-button');
        if (buttons.length > 0) {
            buttons.forEach(button => {
                button.click();
                console.log('Button clicked:', button);
            });
            observer.disconnect();
        } else {
            console.log('Buttons not found, retrying...');
        }
    }

    const observer = new MutationObserver(() => {
        clickAllButtons();
    });

    observer.observe(document, { childList: true, subtree: true });
})();
