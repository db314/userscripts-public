// ==UserScript==
// @name         Wistia player control
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Script that allows you to set a fixed wistia player speed and quality.
// @author       db314
// @match        https://kurs.zrozumiecreact.pl/products/*
// @match        https://learn.mongodb.com/learn/course/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=wistia.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    window._wq = window._wq || [];
    _wq.push({ id: "_all", onReady: function(video) {
        video.playbackRate(2.0)
        video.videoQuality(1080)
    }});
})();